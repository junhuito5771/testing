// BEFORE ADDED SAGA
// import { configureStore } from '@reduxjs/toolkit'

// import { usersReducer } from './reducers';

// const store = configureStore({
//     reducer: {
//         // posts: postsReducer,
//         // comments: commentsReducer,
//         users: usersReducer,
//     },
// })

// AFTER ADD SAGA
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers';
import rootSaga from './sagas';

import { persistStore } from 'redux-persist'


// create the saga middleware
const sagaMiddleware = createSagaMiddleware();
// mount it on the Store
const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware)
);

let persistor = persistStore(store);


// then run the saga
sagaMiddleware.run(rootSaga);

export {
    store,
    persistor
};

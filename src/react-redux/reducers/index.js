import usersReducer from './userReducer';

import { combineReducers } from 'redux';

// ADDED PERSIST REDUCER HERE
import { persistReducer } from 'redux-persist';

import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

const userPersistConfig = {
    key: 'user',
    storage: storage,
    whitelist: ['email'],
};



export default combineReducers({
    usersReducer: persistReducer(userPersistConfig, usersReducer),
});
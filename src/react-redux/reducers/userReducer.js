// BEFORE
// import { createSlice } from '@reduxjs/toolkit';

// export const usersReducer = createSlice({
//   name: 'user',
//   initialState: {
//     email: '',
//   },
//   reducers: {
//     increment: (state) => {
//       // Redux Toolkit allows us to write "mutating" logic in reducers. It
//       // doesn't actually mutate the state because it uses the Immer library,
//       // which detects changes to a "draft state" and produces a brand new
//       // immutable state based off those changes
//       state.email = state.email + ' wow';
//     },
//     decrement: (state) => {
//         state.email = state.email + ' wow';
//     },
//     incrementByAmount: (state, action) => {
//         state.email = state.email + ' wow';
//     },
//   },
// })

// // Action creators are generated for each case reducer function
// export const { increment, decrement, incrementByAmount } = usersReducer.actions;

// export default usersReducer.reducer;

// AFTER
import {
  SET_EMAIL,
  SET_LOADING,
} from '../actions/userAction';

const initialState = {
  email: 'wow',
  isLoading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_EMAIL:
      return {
        ...state,
        email: action.email,
      };
    case SET_LOADING: {
      return {
        ...state,
        isLoading: action.isLoading,
      };
    }
    default:
      return state;
  }
};

export default reducer;

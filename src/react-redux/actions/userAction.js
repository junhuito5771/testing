export const SET_EMAIL = "SET_EMAIL";
export const SET_LOADING = "SET_LOADING";
export const LOGIN_USER = "LOGIN_USER";

export const setUserEmail = email => ({
  type: SET_EMAIL,
  email
});

export const setLoading = isLoading => ({
  type: SET_LOADING,
  isLoading
});

export const loginUser = credentials => ({
  type: LOGIN_USER,
  credentials
});
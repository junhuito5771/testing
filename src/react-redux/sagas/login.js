import { call, put, select, takeLatest } from 'redux-saga/effects';
import {
    SET_EMAIL,
    LOGIN_USER
} from '../actions/userAction';

function* submitLoginForm(action) {
    // DO LOGIN

    // THEN SET SUCCESS MESSAGE
}

export default function* root() {
  yield takeLatest(LOGIN_USER, submitLoginForm);
}

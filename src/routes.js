import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";

import App from "./App";
import UserDetail from "./userDetail";
import NavBar from "./navbar";
import Login from "./login";

function Routers() {
  return (
    <BrowserRouter>
      <NavBar />
      <Routes>
        <Route path="/" element={<App />}/>
        <Route path="/userdetail" element={<UserDetail />}/>
        <Route path="/login" element={<Login/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default Routers;

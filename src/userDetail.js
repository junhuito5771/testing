// import './App.css';

import { connect } from 'react-redux';
import { user } from './react-redux/actions';

function userDetail({email,setEmail}) {
  return (
    <div className='mycontainer'>
      <p>hi</p>
      <br></br>
      <p>My Email = {email}</p>
      <br></br>

      <button onClick={()=>setEmail("nononno")}>Hi</button>
    </div>
  );
}

const mapStateToProps = ({
  usersReducer: {
    email
  },
}) => ({
  email,
});

const mapDispatchToProps = dispatch => ({
  setEmail: params => {
    console.log(params);
    dispatch(user.setUserEmail(params));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(userDetail);

// export default userDetail;
